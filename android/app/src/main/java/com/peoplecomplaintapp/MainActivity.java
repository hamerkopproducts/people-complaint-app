package com.peoplecomplaintapp;

import com.facebook.react.ReactActivity;

import org.devio.rn.splashscreen.SplashScreen;
import android.os.Bundle;

public class MainActivity extends ReactActivity {

  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
    return "PeopleComplaintApp";
  }

  @Override
  protected void onCreate(Bundle SavedInstanceState) {
    SplashScreen.show(this, true);
    super.onCreate(SavedInstanceState);
  }

}
