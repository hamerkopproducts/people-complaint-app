const urls = {
  serverUrl: 'http://hamerkoptest.online',
  prefix: '/api/',
  perPage: 50,
};

export default urls;
