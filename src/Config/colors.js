const colors = {
    BLACK: "#000",
    WHITE: "#FFF",
    DODGER_BLUE: "#428AF8",
    SILVER: "#BEBEBE",
    TORCH_RED: "#F8262F",
    MISCHKA: "#E5E4E6",
    LOGOPINK: '#F66B0D',
    INDIGO: '#D81D92',
    DUNKIN_PINK: '#4050b5',
    GREY:'grey',
    GREEN: 'green',
    PLACEHOLDER: '#828282'
  };
  
  export default colors;