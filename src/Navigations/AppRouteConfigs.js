import React from 'react';
import { createSwitchNavigator } from 'react-navigation';
import { TouchableOpacity } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator, DrawerActions } from 'react-navigation-drawer';
import RouteNames from './RouteNames';

import Intro from '../Screens/Intro';
import SplashScreen from '../Screens/SplashScreen';
import Login from '../Screens/Login';
import AddComplaint from '../Screens/AddComplaint';
import ComplaintList from '../Screens/ComplaintList';
import ComplaintDetail from '../Screens/ComplaintDetail';
import DrawerHeader from '../Components/DrawerHeader';
import BackArrowHeader from '../Components/BackArrowHeader';

import styles from './styles/HeaderStyle';
import LeftPanel from '../Components/LeftPanel';
import { Icon } from 'native-base';
import colors from '../Config/colors';
import { strings } from '../../locales/i18n';

const headerStyles = {
  headerStyle: styles.headerBackground,
  headerTitleStyle: styles.headerTitle,
  headerTintColor: colors.WHITE,
  animationEnabled: true
}

const introStack = createStackNavigator({
  [RouteNames.SplashScreen]: { screen: SplashScreen, navigationOptions: { headerShown: false } },
  [RouteNames.Intro]: {
    screen: Intro, navigationOptions: ({ navigation }) => ({
      headerLeft: "",
      headerTitle: strings('navigation.title.intro'),
      ...headerStyles
    })
  },
  [RouteNames.Login]: {
    screen: Login, navigationOptions: ({ navigation }) => ({
      headerLeft: () => <BackArrowHeader navigation={navigation} />,
      headerTitle: strings('navigation.title.login'),
      ...headerStyles
    })
  },
  [RouteNames.AddComplaint]: {
    screen: AddComplaint, navigationOptions: ({ navigation }) => ({   
      headerLeft: () => <BackArrowHeader navigation={navigation} />,
      headerTitle: strings('navigation.title.add_complaint'),
      ...headerStyles
    })
  },
});


const mainStack = createStackNavigator({
  [RouteNames.ComplaintList]: {
    screen: ComplaintList, navigationOptions: ({ navigation }) => ({
      headerLeft: () => <DrawerHeader navigation={navigation} />,
      headerTitle: strings('navigation.title.complaint_list'),
      ...headerStyles
    })
  },
  [RouteNames.ComplaintDetail]: {
    screen: ComplaintDetail, navigationOptions: (navigation) => ({
      headerTitle: strings('navigation.title.complaint_detail'),
      ...headerStyles
    })
  }
},
  {
    initialRouteName: RouteNames.ComplaintList
  });


const DrawerNavigator = createDrawerNavigator({
  rootStack: mainStack
}, {
  drawerPosition: 'left',
  contentComponent: props => <LeftPanel {...props} />,
}
);
const AppRouteConfigs = createSwitchNavigator(
  {
    [RouteNames.introStack]: { screen: introStack },
    [RouteNames.authStack]: DrawerNavigator
  },
  {
    initialRouteName: RouteNames.introStack,
    resetOnBlur: false,
    backBehavior: 'history'
  },
);

export default AppRouteConfigs;
