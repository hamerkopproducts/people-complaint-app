import {createAppContainer} from 'react-navigation';

import AppRouteConfigs from './AppRouteConfigs';

export const RootStack = createAppContainer(AppRouteConfigs);
