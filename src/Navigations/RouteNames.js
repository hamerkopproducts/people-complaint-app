export default {
  SplashScreen: 'SplashScreen',
  Intro: 'Intro',
  Login: 'Login',
  ComplaintList: 'ComplaintList',
  ComplaintDetail: 'ComplaintDetail',
  AddComplaint: 'AddComplaint',
  TakePicture: 'TakePicture',  
  ComplaintPicture: 'ComplaintPicture',
  authStack: 'AuthStack',
  introStack: 'IntroStack'
};
