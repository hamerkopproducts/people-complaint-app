
import Storage from 'react-native-expire-storage';

// User
const stUserKey = 'user';
export const stGetUser = () =>
  getJsonObjectFromStorage(stUserKey, { onJsonParseError: stRemoveUser });
export const stSaveUser = guest => Storage.setItem(stUserKey, JSON.stringify(guest));
export const stRemoveUser = () => Storage.removeItem(stUserKey);

// Csrf
const stCsrfKey = 'csrf';
export const stGetCsrf = () =>
  getJsonObjectFromStorage(stCsrfKey, { onJsonParseError: stRemoveCsrf });
export const stSaveCsrf = csrf => Storage.setItem(stCsrfKey, JSON.stringify(csrf));
export const stRemoveCsrf = () => Storage.removeItem(stCsrfKey);


// Local functions
const getJsonObjectFromStorage = (key, params = {}) =>
  new Promise(async resolve => {
    const { onJsonParseError } = params;

    try {
      const dataJson = await Storage.getItem(key);
      if (!dataJson) {
        resolve({});
      }
      const data = JSON.parse(dataJson);
      resolve(data);
    } catch (e) {
      onJsonParseError && onJsonParseError();
      resolve({});
    }
  });

export const isEmpty = data => {
  return Object.keys(data).length === 0 ? true : false;
};

export const compare = (a, b) => {
  const nameA = a.name;
  const nameB = b.name;
  let comparison = 0;
  if (nameA > nameB) {
    comparison = 1;
  } else if (nameA < nameB) {
    comparison = -1;
  }
  return comparison;
};

export const formatDate = (date_string = '') => {
  var dateObj = '';
  if (date_string != '') {
    dateObj = new Date(date_string);
  } else {
    dateObj = new Date();
  }
  var month = '' + (dateObj.getMonth() + 1);
  var day = '' + dateObj.getDate();
  const year = dateObj.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;
  return [year, month, day].join('-');
};

export const formatStr = str => {
  let strArr = str.split(' ');
  let formattedString = '';
  for (var i = 0; i < strArr.length; i++) {
    formattedString =
      formattedString +
      ' ' +
      strArr[i].charAt(0).toUpperCase() +
      strArr[i].slice(1).toLowerCase();
  }
  return formattedString;
};
