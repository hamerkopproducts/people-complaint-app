import validator from 'validator';

export const isRequired = (fieldValue, fieldName, msg = '') => {
  let isValid = false;
  let message = '';
  if (fieldValue === undefined || validator.isEmpty(fieldValue)) {
    message = 'Please enter your ' + fieldName;
    if (msg !== '' && msg !== undefined) {
      message = msg;
    }
  } else {
    isValid = true;
  }
  return {isValid, message};
};
