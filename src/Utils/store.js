import store from '../store';

export const getAccessToken = () => store.getState().auth.accessToken || '';


export const getNetworkErrorMsg = () => 'It seems like you are not connected to the ship WiFi network. Please check you WiFi connection and select the current ship again.' || {};
