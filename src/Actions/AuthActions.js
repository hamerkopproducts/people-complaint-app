import { Auth } from './types';
//import { stRemoveUser, stSaveUser} from '../Utils/storage';
import dataService from '../Providers/dataService';

export const loadUserIntoRedux = user => dispatch => {
  dispatch({ type: Auth.USER_LOADED, payload: user });
};

export const logOutUser = onSuccess => async dispatch => {
  try {
    let response = await dataService.fetchUserLogout();
    //stRemoveUser();
    dispatch({ type: Auth.USER_LOG_OUT });
    onSuccess();
  } catch (error) {
    console.log(error);
  }
};

export const loginUser = ({
  username,
  password,
  onSuccess,
}) => async dispatch => {
  try {
    dispatch({ type: Auth.LOGIN_LOADING, payload: true });
    let userData = {
      username: username,
      password: password
    };
    let response = await dataService.fetchLoginUser(userData);
    console.log(response);
    if (response.hasOwnProperty('success') && response.success === true) { 
      if (response.hasOwnProperty('data') && response.data.hasOwnProperty('user')) {
        //stSaveUser(response.data.user);
        dispatch({ type: Auth.LOGIN_SUCCESS, payload: response.data });
        onSuccess(); 
      }   
    } else {
        dispatch({ type: Auth.LOGIN_FAIL, payload: response.user.message });
    }
  } catch (error) {
    console.log(error);
  }
};
