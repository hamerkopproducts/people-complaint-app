import { fetchLoginUser, fetchUserLogout } from './dsLogin';
import { fetchComplaints, fetchComplaintDetail, postComplaint, fetchWards } from './dsComplaints';
const dataService = {
  fetchLoginUser: fetchLoginUser,
  fetchUserLogout: fetchUserLogout,
  fetchComplaints: fetchComplaints,
  fetchComplaintDetail: fetchComplaintDetail,
  fetchComplaints: fetchComplaints,
  postComplaint: postComplaint,
  fetchWards: fetchWards
};

export default dataService;
