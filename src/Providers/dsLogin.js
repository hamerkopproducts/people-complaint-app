import api, {
  getLoginUrl,
  getLogoutUrl
} from '../Api/api';

export const fetchLoginUser = userData =>
  new Promise(async (resolve, reject) => {
    try {
      const url = getLoginUrl();
      const { data } = await api.post(url, userData);
      resolve(data);
    } catch (error) {
      console.log(error);
      reject(error.response);
    }
  });
  
export const fetchUserLogout = () =>
  new Promise(async (resolve, reject) => {
    try {
      const url = getLogoutUrl();
      const { data } = await api.post(url);
      resolve(data);
    } catch (error) {
      console.log(error);
      reject(error.response);
    }
  });
