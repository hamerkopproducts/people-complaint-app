import api, {
  getComplaintUrl,
  getComplaintDetailUrl,
  postComplaintUrl,
  getWardUrl,
  isEmpty,
  isValid
} from '../Api/api';

export const fetchComplaints = (filter = []) =>
  new Promise(async (resolve, reject) => {
    try {
      let url = getComplaintUrl();
      if (!isEmpty(filter)) { 
        if (isValid(filter['date']) && isValid(filter['ward'])) {
          url = url + "?date=" + filter['date'] + "&ward=" + filter['ward'];
        } else {
          if (isValid(filter['date'])) {
            url = url + "?date=" + filter['date'];
          }
          if (isValid(filter['ward'])) {
            url = url + "?ward=" + filter['ward'];
          }
        }
      } 
      const { data } = await api.get(url);
      resolve(data);
    } catch (error) {
      console.log(error);
      reject(error.response);
    }
  });

export const fetchComplaintDetail = (id) =>
  new Promise(async (resolve, reject) => {
    try {
      const url = getComplaintDetailUrl(id);
      console.log(url);
      const { data } = await api.get(url);
      resolve(data);
    } catch (error) {
      console.log(error);
      reject(error.response);
    }
  });

export const postComplaint = (reqData) =>
  new Promise(async (resolve, reject) => {
    try {
      const config = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      };
      const url = postComplaintUrl();
      const { data } = await api.post(url, reqData, config);
      resolve(data);
    } catch (error) {
      console.log(error);
      reject(error.response);
    }
  });

export const fetchWards = () =>
  new Promise(async (resolve, reject) => {
    try {
      const url = getWardUrl();
      console.log(url);
      const { data } = await api.get(url);
      resolve(data);
    } catch (error) {
      console.log(error);
      reject(error.response);
    }
  });