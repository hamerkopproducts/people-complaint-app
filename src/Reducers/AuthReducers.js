import {Auth} from '../Actions/types';

const INITIAL_STATE = {
  user: '',
  accessToken: '',
  displayName: '',
  formErr: '',
  isLoading: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Auth.LOGIN_LOADING:
      return {
        ...state,
        user: '',
        displayName: '',
        formErr: '',
        isLoading: action.payload,
      };
    case Auth.LOGIN_SUCCESS:
      return {
        ...INITIAL_STATE,
        isLoading: false,
        formErr: '',
        user: action.payload.user,
        displayName: action.payload.user.first_name,
        accessToken: action.payload.access_token
      };
    case Auth.LOGIN_FAIL:
      return {
        ...state,
        user: '',
        displayName: '',
        isLoading: false,
        formErr: action.payload,
      };
    case Auth.USER_LOG_OUT:
      return {
        ...INITIAL_STATE,
        user: '',
        displayName: '',
        isLoading: false,
        formErr: action.payload,
      };
    case Auth.USER_LOADED:
      return {
        ...INITIAL_STATE,
        user: action.payload,
        displayName: action.payload.name,
      };
    case Auth.PROFILE_UPDATED:
        return {
          ...state,
          user: action.payload,
          displayName: action.payload.name,
          isLoading: false
        };
    case Auth.PASSWORD_UPDATED:
      return {
        ...state,
        user: action.payload,
        displayName: action.payload.name,
        isLoading: false
      };
    default:
      return {...state};
  }
};
