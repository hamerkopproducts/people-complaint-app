/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component, Suspense } from 'react';
import { RootStack } from './Navigations/AppRoutes';
import { Provider } from 'react-redux';
import { Root, Toast, Drawer } from 'native-base';
import NavigationService from './Providers/NavigationService';
import store from './store';

import SplashScreen from 'react-native-splash-screen';

/*
import FooterView from './Components/Footer';

import iid from '@react-native-firebase/iid';
import messaging from '@react-native-firebase/messaging';


import { isValid } from './Api/api';
import strings from './config/strings';
import styles from './Screens/Styles/ProductList';
import LeftPanel from './Components/LeftPanel'; */



class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      notification: true,
      remoteMessage: ''
    }
  }

  componentDidMount() {
    this.onLoadDefaults();
    SplashScreen.hide();
  }


  onLoadDefaults = async () => {

  };


  render() {
    return (
      <Provider store={store}>
        <Suspense fallback={<p>loading</p>}>
          <Root>
            <RootStack ref={navigatorRef => NavigationService.setTopLevelNavigator(navigatorRef)} />
          </Root>
        </Suspense>
      </Provider>
    );
  }
}

export default App;
