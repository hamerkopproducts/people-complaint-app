import {createStore, applyMiddleware} from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from './Reducers/index';
import {composeWithDevTools} from 'redux-devtools-extension';

const middleware = [ReduxThunk];
/* 
const store = createStore(
  reducers,
  composeWithDevTools(
    applyMiddleware(...middleware),
    // other store enhancers if any
  ),
); */
let composeEnhancers = composeWithDevTools({  trace: true, traceLimit: 25 });
const store = createStore(reducers, composeEnhancers(
  applyMiddleware(...middleware),
  // other store enhancers if any
));

export default store; 