import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Container, Content, Card, CardItem, Thumbnail, Text, DatePicker, Left, Body, Right, Icon, Spinner, View } from 'native-base';

import {Picker} from '@react-native-community/picker';

import styles from './styles/ComplaintList';
import RouteNames from '../Navigations/RouteNames';
import images from '../Assets/images';
import moment from "moment";
import {
  StatusBar,
} from 'react-native';

import { strings } from '../../locales/i18n';
import dataService from '../Providers/dataService';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { isValid } from '../Api/api';

class ComplaintList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      complaints: [],
      chosenDate: new Date(),
      selectedWard: "all",
      wards: [],
      filter: []
    };
    this.setDate = this.setDate.bind(this);
  }

  setDate(newDate) {
    const { filter } = this.state;
    this.setState({ chosenDate: newDate });
    let dateFilter = moment(newDate).format('DD-MM-YYYY');
    filter['date'] = dateFilter;
    this.setState({ filter: filter });
    this.onLoadDefaults(filter);
  }

  setWard = (ward) => {
    const { filter } = this.state;
    this.setState({ selectedWard: ward });
    filter['ward'] = ward;
    this.setState({ filter: filter });
    this.onLoadDefaults(filter);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title', ''),
    };
  };

  handleComplaintDetail = async (item) => {
    const { navigation } = this.props;
    navigation.navigate(RouteNames.ComplaintDetail, {
      title: item.label,
      itemId: item.id,
    });
  };

  onLoadDefaults = async (filter = []) => {
    try {

      this.setState({ isLoading: true });

      dataService.fetchWards().then((response) => {
        let wardsArr = this.onGetWardFilterData(response);
        this.setState({ wards: wardsArr });
      }).catch((error) => {
        console.log(error)
        this.setState({ isLoading: false });
      });

      dataService.fetchComplaints(filter).then((response) => {
        this.onProcessData(response);
      }).catch((error) => {
        console.log(error)
        this.setState({ isLoading: false });
      });

    } catch (error) {
      this.setState({ isLoading: false });
    }
  };
  onGetWardFilterData = response => {
    try {
      if (response.data && response.data.length > 0) {
        let valueArr = [];
        let data = response.data;
        data.map(item => {
          if (item.lang.length > 0) {
            let temp = {};
            temp.id = item.id;
            temp.name = item.lang[0].name;
            temp.text = item.lang[0].name;
            valueArr.push(temp);
          }
        });
        return valueArr;
      }
    } catch (error) {
      console.log(error);
    }
  };

  onProcessData = response => {
    try {
      let complaints = { list: [] };
      if (response.data) {
        complaints.list = response.data.data;
        this.setState({ isLoading: false });
        this.setState({ complaints: complaints });
      }

    } catch (error) {
      console.log(error);
    }
  };

  resetFilter = async () => {
    this.setState({ filter: [] });
    this.setState({ selectedWard: "all" });
    this.setState({ chosenDate: new Date() });
    this.onLoadDefaults();
  }


  componentDidMount() {
    const { displayName, navigation } = this.props;
    if (isValid(displayName)) {
      this.onLoadDefaults();
    } else {
      navigation.navigate(RouteNames.Login);
    }
  }

  render() {
    let { complaints, isLoading, wards, selectedWard } = this.state;
    let defaultDate = new Date();
    var date = new Date();
    let minimumDate = date.setMonth(date.getMonth() - 10);
    let cardItems = [];
    let wardOptions = [];
    let today = moment();
    if (wards?.length > 0) {
      wardOptions.push(
        <Picker.Item label={strings('complaintList.ward_placeholder')} value="all" />
      );
      wards.map((item, index) => {
        wardOptions.push(
          <Picker.Item label={item.name} value={item.id} />
        );
      });
    }

    if (complaints?.list?.length > 0) {
      complaints.list.map((item, index) => {
        let created_at = moment(item.created_at).format('dddd');
        var diff = moment(today).diff(created_at, 'days');
        if (diff > 5) {
          created_at = moment(item.created_at).format('Do MMMM');
        }
        cardItems.push(
          <TouchableOpacity style={styles.container} onPress={this.handleComplaintDetail.bind(this, item)}>
            <Card style={{ flex: 0 }} key={index}>
              <CardItem>
                <Left>
                  <Thumbnail square source={{ uri: item.file }} onPress={this.handleComplaintDetail.bind(this, item)} />
                  <Body >
                    <View style={styles.cardstyle} >
                      <Text style={styles.detailText}>{item.title}</Text>
                      {(item['users'] !== undefined) &&
                        <Text note>{item['users'].name}</Text>
                      }
                    </View>
                  </Body>
                </Left>
                <Right>
                  <Text note>{created_at}</Text>
                </Right>
              </CardItem>
            </Card>
          </TouchableOpacity>);
      });
    }
    return (
      <Container style={styles.container}>
        <StatusBar barStyle="light-content"
          backgroundColor={"#303F9F"} />
        <Content style={styles.content}>
          <Card style={{ flex: 0 }}>
            <CardItem>
              <Icon name="calendar" />
              <DatePicker
                defaultDate={defaultDate}
                minimumDate={minimumDate}
                maximumDate={defaultDate}
                locale={"en"}
                timeZoneOffsetInMinutes={undefined}
                modalTransparent={false}
                animationType={"fade"}
                androidMode={"default"}
                placeHolderText={strings('complaintList.search')}
                textStyle={styles.dateText}
                placeHolderTextStyle={styles.datePlaceHolderText}
                onDateChange={this.setDate}
                disabled={false}
              />
            </CardItem>
            <CardItem>
              <Icon name="md-location" />
              <Picker
                note
                mode="dropdown"
                style={{ width: 120 }}
                selectedValue={selectedWard}
                onValueChange={this.setWard}
              >
                {wardOptions}
              </Picker>
              <Icon name="md-refresh-circle-outline" onPress={this.resetFilter} ></Icon>
            </CardItem>
          </Card>
          {(isLoading === true) && <Spinner />}
          {cardItems}
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = ({ auth }) => {
  return { ...auth };
};

export default connect(
  mapStateToProps,
  {},
)(ComplaintList);


