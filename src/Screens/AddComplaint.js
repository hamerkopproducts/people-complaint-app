import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image, View, Text, KeyboardAvoidingView, Platform, TouchableWithoutFeedback, Keyboard } from 'react-native';

import PersonalInformation from './PersonalInformation';
import RouteNames from '../Navigations/RouteNames';
import { isRequired } from '../Utils/validators';
import { isValid } from '../Api/api';
import { Spinner, Toast, Tabs, Tab, TabHeading, Icon, Content, Container } from 'native-base';
import styles from './styles/AddComplaint';
import ComplaintForm from './ComplaintForm';
import { strings } from '../../locales/i18n';
import dataService from '../Providers/dataService';

class AddComplaint extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initialPage: 0,
      activeTab: 0,
      complaint: [],
      formErr: '',
      isLoading: false
    };
  }


  handleValidation = () => {
    let errFlag = 0;
    let errorText = "";
    let { complaint } = this.state;
    let form_keys = ['address', 'house_no', 'phone', 'last_name', 'first_name', 'file', 'description', 'title','ward_id'];
    form_keys.map((item) => {
      errorText = item + '_err';
      if (!complaint.hasOwnProperty(item)) {
        errFlag = 1;
        this.setState({ formErr: errorText });
        return true;
      }
      if (complaint.hasOwnProperty(item) && complaint[item] === '') {
        errFlag = 1;
        return true;
      }
    });

    if (errFlag === 1) {
      this.setState({ formErr: errorText });
    } else {
      this.setState({ formErr: "" });
    }

    return errFlag;

  };

  submitComplaint = async (complaintObj) => {
    try {
      this.setState({ isLoading: true });
      //e.preventDefault();
      let { complaint } = this.state;
      const { navigation } = this.props;
      complaint.title = complaintObj.title;
      complaint.description = complaintObj.description;
      complaint.file = complaintObj.file;
      this.setState({ complaint: complaint });
      let errFlag = this.handleValidation();
      if (errFlag === 0) {
        let postData = this.processComplaint();
        dataService.postComplaint(postData).then((response) => {
          this.setState({ isLoading: false });
          Toast.show({
            text: strings('complaintForm.success_message'),
            position: "bottom",
            duration: 3000,
            style: styles.toastWrapper,
            buttonTextStyle: styles.toastText,
            buttonStyle: styles.toastBtn,
          });
          navigation.navigate(RouteNames.Intro);
        }).catch((error) => {
          console.log(error)
          this.setState({ isLoading: false });
        });
      }
    } catch (error) {
      console.log(error);
      this.setState({ isLoading: false });
    }
  };

  processComplaint = () => {
    let { complaint } = this.state;
    const data = new FormData();
    data.append('first_name', complaint.first_name);
    data.append('last_name', complaint.last_name);
    data.append('phone', complaint.phone);
    data.append('house_no', complaint.house_no);
    data.append('address', complaint.address);
    data.append('title', complaint.title);
    data.append('description', complaint.description);
    data.append('ward_id', complaint.ward_id);
    const ext = complaint.file.uri.substring(complaint.file.uri.lastIndexOf('.'));
    data.append('file', {
      name: complaint.file.fileName + '-' + ext,
      type: complaint.file.type,
      uri: Platform.OS == 'android' ? complaint.file.uri : complaint.file.uri.replace("file://", "")
    });

    return data;
  }


  componentDidMount() {
     
  }

  proceedToComplaint = async (userObj) => {
    this.setState({ complaint: userObj });
    this.setState({ activeTab: 1 });
  }

  render() {
    const { isLoading } = this.state;
    return (
      <KeyboardAvoidingView
        style={styles.containerwrapper}
        behavior={Platform.OS == "ios" ? "padding" : "height"}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <Container>
            <Content>
                <Tabs initialPage={this.state.initialPage} page={this.state.activeTab}>
                <Tab heading={<TabHeading>
                  <Icon name="person" />
                  <Text style={styles.tabHeaderText}>{strings('addComplaint.personal_information')}</Text>
                </TabHeading>}>
                  <PersonalInformation execFromParent={this.proceedToComplaint} />
                </Tab>
                <Tab heading={<TabHeading>
                  <Icon name="ios-mail-unread" />
                  <Text style={styles.tabHeaderText}>{strings('addComplaint.complaint')}</Text>
                </TabHeading>}>
                  <ComplaintForm execFromParent={this.submitComplaint} onProgress={isLoading} />
                </Tab>
              </Tabs>
            </Content>
          </Container>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  return { ...auth };
};

export default connect(
  mapStateToProps, {},
)(AddComplaint);
