import React, { Component } from 'react';
import { Text, Container, Content, Item, Input, Label, Form, Button, InputGroup, Icon, ActionSheet, View, Toast } from 'native-base';
import styles from './styles/PersonalInformation';
import RouteNames from '../Navigations/RouteNames';
import { strings } from '../../locales/i18n';
import dataService from '../Providers/dataService';

var DESTRUCTIVE_INDEX = 3;
var CANCEL_INDEX = 4;

class PersonalInformation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userDetails: [],
            wards: [],
            selectedWard: {},
            formErr: '',
            isLoading: false
        };
    }

    onLoadDefaults = async () => {
        try {
            let { userDetails } = this.state;
            let wards = { list: [] };
            let response = await dataService.fetchWards();
            wards.list = this.onProcessData(response);
            this.setState({ wards: wards.list });
            if (wards.list.length) {
                this.setState({ selectedWard: wards.list[0] });
                userDetails['ward_id'] = wards.list[0].id;
                this.setState({ userDetails: userDetails });
            }
        } catch (error) {
            console.log(error);
        }
    }

    onProcessData = response => {
        try {
            if (response.data && response.data.length > 0) {
                let valueArr = [];
                let data = response.data;
                data.map(item => {
                    if (item.lang.length > 0) {
                        let temp = {};
                        temp.id = item.id;
                        temp.name = item.lang[0].name;
                        temp.text = item.lang[0].name;
                        valueArr.push(temp);
                    }
                });
                return valueArr;
            }
        } catch (error) {
            console.log(error);
        }
    };

    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title', ''),
        };
    };

    componentDidMount() {
        this.onLoadDefaults();
    }

    onFormValidation = () => {
        try {
            let errFlag = 0;
            let errorText = "";
            let { userDetails } = this.state;
            if (!userDetails.hasOwnProperty('ward_id')) {
                errorText = 'ward_err';
                errFlag = 1;
                Toast.show({
                    text: strings('personalInformation.ward_error'),
                    buttonText: strings('complaintForm.toast_filesize_button'),
                    position: "bottom",
                    duration: 3000,
                    style: styles.toastWrapper,
                    buttonTextStyle: styles.toastText,
                    buttonStyle: styles.toastBtn,
                });
                return true;
            }
            if (userDetails.hasOwnProperty('ward_id') && userDetails['ward_id'] == "") {
                errorText = 'ward_err';
                errFlag = 1;
            }
            let form_keys = ['address', 'house_no', 'phone', 'last_name', 'first_name'];
            form_keys.map((item) => {
                if (!userDetails.hasOwnProperty(item)) {
                    errFlag = 1;
                    errorText = item + '_err';
                    return true;
                }
                if (userDetails.hasOwnProperty(item) && userDetails[item] === '') {
                    errorText = item + '_err';
                    errFlag = 1;
                    return true;
                }
                if (item === 'phone') {
                    if (userDetails.hasOwnProperty(item) && userDetails[item] !== "") {
                        var pattern = new RegExp(/^[0-9\b]+$/);
                        if (!pattern.test(userDetails[item])) {
                            errorText = item + '_err';
                            errFlag = 1;
                        } else if (userDetails[item].length !== 10) {
                            errorText = item + '_err';
                            errFlag = 1;
                        }
                    }
                }
            });


            if (errFlag === 1) {
                this.setState({ formErr: errorText });
            } else {
                this.setState({ formErr: "" });
            }
            return errFlag;
        } catch (error) {
            console.log(error);
        }
    }

    handleProceed = () => {
        try {
            let errFlag = this.onFormValidation();
            if (errFlag === 0) {
                let { userDetails } = this.state;       
                this.props.execFromParent && this.props.execFromParent(userDetails);
            }
        } catch (error) {
            console.log(error);
        }
    }
    onFormValueChange = (value, field_name) => {
        let { userDetails } = this.state;
        userDetails[field_name] = value;
        this.setState({ userDetails: userDetails });
    }

    handleWardChange = (itemValue, itemIndex) => {
        if (!itemIndex) return;

        let { userDetails, wards } = this.state;

        if (wards.length > 0) {
            const selectedWard = wards.find(item => {
                return item.id === itemIndex.id;
            });
            this.setState({ selectedWard: selectedWard });
            userDetails['ward_id'] = selectedWard.id;
            this.setState({ userDetails: userDetails });
        }
    };

    render() {
        let { userDetails, formErr, wards, selectedWard } = this.state;

        return (
            <Container style={styles.container}>
                <Content contentContainerStyle={{ flex: 1 }} style={styles.content}>
                    <Form>                           
                        <Item stackedLabel error={(formErr == 'ward_err') ? true : false}>
                        <Label>{strings('personalInformation.ward')}</Label>
                        {wards.length > 0 &&
                            <View style={styles.actionPicker} onStartShouldSetResponder={() =>
                                ActionSheet.show(
                                    {
                                        options: wards,
                                        cancelButtonIndex: CANCEL_INDEX,
                                        destructiveButtonIndex: DESTRUCTIVE_INDEX,
                                        title: strings('personalInformation.ward')
                                    },
                                    buttonIndex => {
                                        this.handleWardChange(this, wards[buttonIndex])
                                    }
                                )}>
                                <Text style={styles.titleCase}>{selectedWard?.name}</Text>
                                <Icon name='chevron-down' />
                            </View>
                        } 
                        </Item>
                        <Item stackedLabel error={(formErr == 'first_name_err') ? true : false}>
                            <Label>{strings('personalInformation.first_name')}</Label>
                            <Input value={userDetails.first_name} onChangeText={(text) => this.onFormValueChange(text, 'first_name')} />
                        </Item>
                        <Item stackedLabel error={(formErr == 'last_name_err') ? true : false}>
                            <Label>{strings('personalInformation.last_name')}</Label>
                            <Input value={userDetails.last_name} onChangeText={(text) => this.onFormValueChange(text, 'last_name')} />
                        </Item>
                        <Item stackedLabel error={(formErr == 'phone_err') ? true : false}>
                            <Label>{strings('personalInformation.phone')}</Label>
                            <Input value={userDetails.phone} onChangeText={(text) => this.onFormValueChange(text, 'phone')} />
                        </Item>
                        <Item stackedLabel error={(formErr == 'house_no_err') ? true : false}>
                            <Label>{strings('personalInformation.house_no')}</Label>
                            <Input value={userDetails.house_no} onChangeText={(text) => this.onFormValueChange(text, 'house_no')} />
                        </Item>
                        <Item stackedLabel error={(formErr == 'address_err') ? true : false}>
                            <Label>{strings('personalInformation.address')}</Label>
                            <InputGroup borderType='regular'>
                                <Input value={userDetails.address} multiline={true} onChangeText={(text) => this.onFormValueChange(text, 'address')} />
                            </InputGroup>
                        </Item>

                        <View style={styles.nextstyle}>
                            <Button rounded onPress={this.handleProceed.bind(this)} style={styles.Nextbutton}>
                                <Text>{strings('personalInformation.button_text')}</Text>
                            </Button>
                        </View>

                    </Form>
                </Content>
            </Container>
        )
    }
}
export default PersonalInformation;
