import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image, View, Text, KeyboardAvoidingView, Platform, TouchableWithoutFeedback, Keyboard,  StatusBar,
} from 'react-native';

import colors from '../Config/colors';
import { strings } from '../../locales/i18n';
import FormTextInput from '../Components/FormTextInput';
import Button from '../Components/Button';
import imageLogo from '../Assets/Images/splash.png';
import RouteNames from '../Navigations/RouteNames';
import { isRequired } from '../Utils/validators';
import { loginUser, loadUserIntoRedux } from '../Actions';
import { isValid } from '../Api/api';
//import { stGetUser, isEmpty } from '../Utils/storage';
import { Spinner } from 'native-base';
import styles from './styles/Login';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: 'complaints',
      usernameErr: '',
      password: '123456',
      passwordErr: '',
      formErr: '',
      isLoading: false
    };
  }

  handleUsernameChange = username => {
    this.setState({ username: username });
    this.setState({ isLoading: false });
  };

  handlePasswordChange = password => {
    this.setState({ password: password });
    this.setState({ isLoading: false });
  };

  handleValidation = () => {
    const { username, password } = this.state;
    const userNameValidator = isRequired(username, 'username');
    const passwordValidator = isRequired(password, 'password');

    if (!userNameValidator.isValid) {
      this.setState({ usernameErr: userNameValidator.message });
    } else {
      this.setState({ usernameErr: '' });
    }
    if (!passwordValidator.isValid) {
      this.setState({ passwordErr: passwordValidator.message });
    } else {
      this.setState({ passwordErr: '' });
    }
    if (userNameValidator.isValid && passwordValidator.isValid) {
      return true;
    } 
    return false;
  };

  handleLoginPress = e => {
    this.setState({ isLoading: true });
    e.preventDefault();
    const { username, password } = this.state;
    try {
      if (this.handleValidation()) {
          this.props.loginUser({
          username: username,
          password: password,
          onSuccess: () => {
            this.nextStep();
          },
        });  
      }
    } catch (error) {
      this.setState({ isLoading: false });
    }
  };

  nextStep = () => {
    this.setState({ isLoading: false });
    const { navigation } = this.props;
    navigation.navigate(RouteNames.authStack);
  };

  componentDidMount() {
    const { displayName } = this.props;
    if (isValid(displayName)) {
      this.nextStep();
    } else {
      this.onLoadDefaults();
    }
  }

  onLoadDefaults = async () => {
/*     const userObj = await stGetUser();
    if (!isEmpty(userObj)) {
      await this.props.loadUserIntoRedux(userObj);
      await this.nextStep();
    } */
  };

  render() {
    const { isLoading } = this.props;
    return (
      <KeyboardAvoidingView
        style={styles.containerwrapper}
        behavior={Platform.OS == "ios" ? "padding" : "height"}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={styles.container}>
            <Image source={imageLogo} style={styles.logo} />
            <View style={styles.form}>
              {isLoading && <Spinner color={colors.GREEN} />}
              <Text style={styles.errText}>{this.props.formErr}</Text>
              <FormTextInput placeholderTextColor={colors.PLACEHOLDER} style={styles.fInputs}
                value={this.state.username}
                onChangeText={this.handleUsernameChange}
                placeholder={strings('login.username')}
              />
              <Text style={styles.errText}>{this.state.usernameErr}</Text>
              <FormTextInput placeholderTextColor={colors.PLACEHOLDER} style={styles.fInputs}
                value={this.state.password}
                secureTextEntry={true}
                onChangeText={this.handlePasswordChange}
                placeholder={strings('login.password')}
              />
              <Text style={styles.errText}>{this.state.passwordErr}</Text>
              <Button label={strings('login.button_text')} onPress={this.handleLoginPress} />
            </View>
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
      
    );
  }
}
const mapStateToProps = ({ auth }) => {
  return { ...auth };
};

export default connect(
  mapStateToProps,
  {
    loginUser,
    loadUserIntoRedux,
  },
)(Login);
