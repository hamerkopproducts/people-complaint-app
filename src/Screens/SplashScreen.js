import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image, UIManager, View, Text,  StatusBar,
} from 'react-native';

import colors from '../Config/colors';
import strings from '../Config/strings';
import Button from '../Components/Button';
import axios from 'axios';
import imageLogo from '../Assets/Images/splash.png';
import RouteNames from '../Navigations/RouteNames';
//import { logOutUser,loadUserIntoRedux } from '../Actions';
import styles from './styles/SplashScreen';
//import {stGetUser, isEmpty} from '../Utils/storage';

class SplashScreen extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.configureLayoutAnimation();
    this.configureAxios();
    this.onLoadDefaults();
  }

  configureLayoutAnimation() {
    try {
      //if (Config.isAndroid) {
      UIManager.setLayoutAnimationEnabledExperimental &&
        UIManager.setLayoutAnimationEnabledExperimental(true);
      //}
    } catch (error) {
      console.log(error);
    }
  }

  configureAxios() {
    try {
      //const { logOutUser } = this.props;
      axios.interceptors.response.use(
        response => response,
        error => {
          if (error.response && error.response.status === 401) {
            Config.logGeneral && console.log('Unauthorized request, logging out ...');
            //logOutUser(this.initialStep);
          }
          return Promise.reject(error);
        }
      );
    } catch (error) {
      console.log(error);
    }
  }

  initialStep = () => {
    try {
      const { navigation } = this.props;
      navigation.navigate(RouteNames.SplashScreen);
    } catch (error) {
      console.log(error);
    }
  };

  /*   nextStep = () => {
      const { navigation } = this.props;
      navigation.navigate(RouteNames.navStack, {
        title: 'Region',
      });
    }; */

  onLoadDefaults = async () => {
     this.introScreen();
    /*  const userObj = await stGetUser();
     if (!isEmpty(userObj)) {
       this.props.loadUserIntoRedux(userObj);
       this.nextStep();
     } else {
       this.handleLoginPress();
     } */
  };

  introScreen = () => {
    try {
      const { navigation } = this.props;
      navigation.navigate(RouteNames.Intro, {
        title: '',
      });
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    return (
      <View style={styles.container}>            
        <StatusBar barStyle="light-content"
				backgroundColor={"#303F9F"} />
        <Image source={imageLogo} style={styles.logo} />
      </View>
    );
  }
}


const mapStateToProps = ({  auth }) => {
  return { ...auth };
};

export default connect(
  mapStateToProps,
  {
    /*     logOutUser,
        loadUserIntoRedux */
  },
)(SplashScreen);

