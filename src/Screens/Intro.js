import React, { Component } from 'react';
import { Text, Container, Content, Grid, Row, Button, Header, Body, Title } from 'native-base';
import styles from './styles/Intro';
import RouteNames from '../Navigations/RouteNames';

import { strings } from '../../locales/i18n';

class Intro extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title', ''),
    };
  };

  componentDidMount() {

  }

  handleLogin = () => {
    const { navigation } = this.props;
    navigation.navigate(RouteNames.Login);
  };

  handleComplaint = () => {
    const { navigation } = this.props;
    navigation.navigate(RouteNames.AddComplaint);
  };


  render() {

    return (
      <Container style={styles.container}>
        <Content contentContainerStyle={{ flex: 1 }} style={styles.content}>
          <Body style={styles.body}>

          <Button  primary rounded onPress={this.handleComplaint} style={styles.button}>
              <Text>{strings('intro.add_complaint')}</Text>
            </Button>
            <Button warning rounded onPress={this.handleLogin} style={styles. buttonlogin}>
              <Text >{strings('intro.login')}</Text>
            </Button>
          </Body>
        </Content>
      </Container>
    )
  }
}

export default Intro;
