import React, { Component } from 'react';
import { Image } from 'react-native';
import { Container, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right, View } from 'native-base';
import styles from './styles/ComplaintDetail';
import RouteNames from '../Navigations/RouteNames';
import images from '../Assets/images';
import { strings } from '../../locales/i18n';
import dataService from '../Providers/dataService';
import moment from "moment";

class ComplaintDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            complaint: []
        };
    }

    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title', ''),
        };
    };

    onLoadDefaults = async () => {
        try {
            const { navigation } = this.props;
            let itemId = navigation.getParam('itemId', '');
            if (itemId !== "") {
                dataService.fetchComplaintDetail(itemId).then((response) => {
                    this.setState({ isLoading: false });
                    this.onProcessData(response);
                }).catch((error) => {
                    console.log(error)
                    this.setState({ isLoading: false });
                });
            }

        } catch (error) {
            this.setState({ isLoading: false });
        }
    };

    onProcessData = response => {
        try {
            let complaint = {};
            if (response.data) {
                complaint = response.data;
            }
            this.setState({ complaint: complaint });
        } catch (error) {
            console.log(error);
        }
    };

    componentDidMount() {
        this.onLoadDefaults();
    }

    render() {
        let { complaint } = this.state;
        var createdDate = moment(complaint.created_at).format('DD/MMM/YYYY');
        return (
            
            <Container style={styles.container}>
                <Content style={styles.content}>
                    <Card style={{ flex: 0 }}>
                        <CardItem header>
                            <Left>
                                <Text style={styles.titlestyle}>{complaint.title}</Text>
                            </Left>
                        </CardItem>
                        <CardItem >
                            <Left>
                                {(complaint['users'] !== undefined) &&
                                    <Body>
                                        <View style={styles.titleview}>
                                       <Text note>{strings('complaintDetail.name')}</Text>
                                        <Text >{complaint.users.first_name} {complaint.users.last_name}</Text>
                                        </View>
                                        <View style={styles.titleview}>
                                        <Text note>{strings('complaintDetail.house_no')}</Text>
                                        <Text >{complaint.users.house_no}</Text>
                                        </View>
                                        <View style={styles.titleview}>
                                        <Text note>{strings('complaintDetail.address')}</Text>
                                        <Text >{complaint.users.address}</Text>
                                        </View>
                                        <View style={styles.titleview}>
                                        <Text note>{strings('complaintDetail.phone')}</Text>
                                        <Text >{complaint.users.phone}</Text>
                                        </View>
                                    </Body>
                                }
                            </Left>
                            <Right>
                                <Body>
                                <View style={styles.titleview}>
                                <Text note>{strings('complaintDetail.status')}</Text>
                                    <Text >{complaint.status}</Text>
                                    </View>
                                    <View style={styles.titleview,{marginLeft:"20%"}}>
                                    <Text note>{strings('complaintDetail.date')}</Text>
                                    <Text >{createdDate}</Text>
                                    </View>
                                </Body>
                            </Right>
                        </CardItem>
                        <CardItem>
                            <Left>
                                <Body>
                                    <Image source={{ uri: complaint.file }} style={styles.actualImage} />
                                    <View style={styles.titleview,{width:"100%",marginTop:"3%"}}>
                                    <Text note>{strings('complaintDetail.description')} : </Text>
                                    <Text>{complaint.description}</Text>
                                    </View>
                                </Body>
                            </Left>
                        </CardItem>
                    </Card>
                </Content>
            </Container>
        )
    }
}

export default ComplaintDetail;
