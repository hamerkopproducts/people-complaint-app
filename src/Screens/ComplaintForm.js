import React, { Component } from 'react';
import { Text, Container, Content, Item, Input, Label, Form, Spinner, Icon, Button, InputGroup, Toast, View } from 'native-base';
import styles from './styles/ComplaintForm';
import RouteNames from '../Navigations/RouteNames';
import ImagePicker from 'react-native-image-picker';
import { Image } from 'react-native';
import { strings } from '../../locales/i18n';

class ComplaintForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            complaintDetails: [],
            formErr: '',
            imageUrl: '',
            isLoading: false
        };
    }

    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title', ''),
        };
    };

    componentDidMount() {

    }

    onFormValidation = () => {
        try {
            let errFlag = 0;
            let errorText = "";
            let { complaintDetails } = this.state;
            let form_keys = ['file', 'description', 'title'];
            form_keys.map((item) => {
                if (!complaintDetails.hasOwnProperty(item)) {
                    errFlag = 1;
                    errorText = item + '_err';
                    return true;
                }
                if (complaintDetails.hasOwnProperty(item) && complaintDetails[item] === '') {
                    errorText = item + '_err';
                    errFlag = 1;
                    return true;
                }
            });

            if (errFlag === 1) {
                this.setState({ formErr: errorText });
            } else {
                this.setState({ formErr: "" });
            }
            return errFlag;
        } catch (error) {
            console.log(error);
        }
    }

    addImage = async () => {
        let { complaintDetails } = this.state;
        const options = {
            title: strings('complaintForm.image_select'),
            cropping: true,
            maxWidth: 600,
            maxHeight: 480,
            quality: .5,
            storageOptions: {
              skipBackup: true,
              path: 'complaint_app_images',
            }
        };
        try {
            ImagePicker.showImagePicker(options, (res) => {
                if (res.didCancel) {
                    console.log('User cancelled image picker');
                } else if (res.error) {
                    console.log('ImagePicker Error: ', res.error);
                } else {
                    if (res.fileSize / 1000000 > 10) {
                        Toast.show({
                            text: strings('complaintForm.filesize_error'),
                            buttonText: strings('complaintForm.toast_filesize_button'),
                            position: "bottom",
                            duration: 3000,
                            style: styles.toastWrapper,
                            buttonTextStyle: styles.toastText,
                            buttonStyle: styles.toastBtn,
                        });
                        return false;
                    }
                    let contains = res.path.includes('file:///')
                    let filePath = contains ? res.path : res.path.replace('file:///', "file://");
                    const ext = res.uri.substring(res.uri.lastIndexOf('.'));
                    this.setState({
                        img: res,
                        imgUrl: null,
                        selected: true,
                        cropped: false,
                        typeImage: res.type,
                        nameImage: res.fileName + '-' + ext,
                        imageUrl: res.uri
                    });
                    complaintDetails['file'] = res;
                    this.setState({ complaintDetails: complaintDetails });
                }
            });

        } catch (err) {
            console.log(err);
        }
    }

    onFormValueChange = (value, field_name) => {
        let { complaintDetails } = this.state;
        complaintDetails[field_name] = value;
        this.setState({ complaintDetails: complaintDetails });
    }


    handleSubmit = () => {
        try {
            this.setState({ isLoading: true });
            let errFlag = this.onFormValidation();
            if (errFlag === 0) {
                let { complaintDetails } = this.state;
                this.props.execFromParent && this.props.execFromParent(complaintDetails);
            } else {
                this.setState({ isLoading: false });
            }
        } catch (error) {
            console.log(error);
            this.setState({ isLoading: false });
        }
    }
    render() {
        let { complaintDetails, imageUrl, formErr, isLoading } = this.state;
        let { onProgress } = this.props;
        return (
            <Container style={styles.container}>
                {(isLoading === true) && <Spinner />}
                <Content contentContainerStyle={{ flex: 1 }} style={styles.content}>
                    <Form>
                        <Item stackedLabel error={(formErr == 'title_err') ? true : false}>
                            <Label>{strings('complaintForm.title')}</Label>
                            <Input value={complaintDetails.title} onChangeText={(text) => this.onFormValueChange(text, 'title')} />
                        </Item>
                        <Item stackedLabel error={(formErr == 'description_err') ? true : false}>
                            <Label>{strings('complaintForm.description')}</Label>
                            <InputGroup borderType='regular'>
                                <Input value={complaintDetails.description} multiline={true} onChangeText={(text) => this.onFormValueChange(text, 'description')} />
                            </InputGroup>
                        </Item>
                        <Item>
                            <Label>{strings('complaintForm.image')}</Label>
                            <Button transparent>
                                <Icon name='camera' style={(formErr == 'file_err') ? styles.errIcon : ""} onPress={() => this.addImage()} />
                            </Button>
                        </Item>
                        {(imageUrl !== '') &&
                            <Item>
                                <Image source={{ uri: imageUrl }} style={styles.actualImage} />
                            </Item>
                        }

                        <View style={styles.nextstyle}>
                            <Button rounded onPress={this.handleSubmit} disabled={(isLoading === true) ? true : false}>
                                <Text>{strings('complaintForm.submit')}</Text>
                            </Button>
                        </View>

                    </Form>
                </Content>
            </Container>
        )
    }
}

export default ComplaintForm;
