import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    actionPicker: {
        width: '60%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    pickerStyle: {
        width: '50%',
        transform: [
            { scaleX: 1.5 },
            { scaleY: 1.5 },
        ],
        textTransform: 'capitalize'
    },

    pickerItem: {
        height: 65,
        transform: [
            { scaleX: 1.5 },
            { scaleY: 1.5 },
        ]
    },

    titleCase: {
        textTransform: 'capitalize',
        fontSize: 16
    },
    content: {
        padding: 20
    },
    body: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        margin: 10,
        justifyContent: 'center',
        width: 200,
        alignItems: 'center'
    },
    buttonlogin: {
        margin: 10,
        justifyContent: 'center',
        width: 200,
        alignItems: 'center'
    },
    nextstyle: {
        marginVertical: "8%",
        marginLeft: "65%"
    },
    Nextbutton: {
        margin: 10,
        justifyContent: 'center',
        width: 110,
        alignItems: 'center'
    },
});

export default styles;