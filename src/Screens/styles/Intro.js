import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    padding: 20
  },
  body: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  button: {
    margin: 10,
    justifyContent: 'center',
    width: 200,
    alignItems: 'center'
  },
  buttonlogin: {
    margin: 10,
    justifyContent: 'center',
    width: 200,
    alignItems: 'center'
  },
  nextstyle: {
    marginVertical: "8%",
    marginLeft: "65%"
  },
  Nextbutton: {
    margin: 10,
    justifyContent: 'center',
    width: 110,
    alignItems: 'center'
  },
});

export default styles;