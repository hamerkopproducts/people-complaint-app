import { StyleSheet } from 'react-native';
import colors from "../../Config/colors";

const styles = StyleSheet.create({
  toastWrapper: {
    backgroundColor: colors.GREY,
  },
  toastText: {
    color: colors.WHITE,
    textTransform: 'capitalize'
  },
  toastBtn: {
    backgroundColor: colors.DUNKIN_PINK,
  },
  actualImage: {
    alignItems: "center",
    justifyContent: "center",
    height: 200,
    width: 320,
    flex: 1,
    flexDirection: "column"
  },
  errIcon: {
    color: 'red'
  },
  nextstyle:{
    marginVertical:"10%",
    marginLeft:"60%",
    
  }
});

export default styles;