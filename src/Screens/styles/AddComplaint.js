import { StyleSheet } from 'react-native';
import colors from "../../Config/colors";

const styles = StyleSheet.create({
  containerwrapper: {
    flex: 1
  },
  tabHeaderText: {
    color: colors.WHITE,
    padding: 5,
    fontWeight: "bold",
    justifyContent:"space-around",    
  },
  toastWrapper: {
    backgroundColor: colors.GREY,
  },
  toastText: {
    color: colors.WHITE,
    textTransform: 'capitalize'
  },
  toastBtn: {
    backgroundColor: colors.DUNKIN_PINK,
  },
});

export default styles;