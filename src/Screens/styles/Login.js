import { StyleSheet } from 'react-native';
import colors from "../../Config/colors";

const styles = StyleSheet.create({
  containerwrapper: {
    flex: 1
  },
  container: {
    flex: 1,
    backgroundColor: colors.WHITE,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  fInputs: {
    fontSize: 24,
    fontWeight: '900'
  },
  logo: {
    flex: 1,
    width: '50%',
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  form: {
    flex: 1,
    justifyContent: 'center',
    width: '80%',
    marginBottom: 32
  },
  errText: {
    fontSize: 16,
    color: 'red'
  }
});

export default styles;