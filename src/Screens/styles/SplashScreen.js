import { StyleSheet } from 'react-native';
import colors from "../../Config/colors";

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: colors.WHITE,
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    imageContainer: {
      flex: 1,
      backgroundColor: colors.WHITE,
  
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    logo: {
      flex: 1,
      width: '100%',
      resizeMode: 'contain',
      alignSelf: 'center',
    },
    form: {
      flex: 1,
      justifyContent: 'center',
      width: '80%',
    },
  });
  
  export default styles;