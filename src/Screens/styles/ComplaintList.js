import { StyleSheet } from 'react-native';
import colors from "../../Config/colors";

const styles = StyleSheet.create({
  dateText: {
    color: colors.DUNKIN_PINK,
  },
  datePlaceHolderText: {
    color: colors.PLACEHOLDER,
  }
  ,
  detailText: {
    width:"150%",
  },
  cardstyle: {
    height:80,
    marginTop:"15%"
  }
});

export default styles;