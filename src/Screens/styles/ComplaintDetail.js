import { StyleSheet } from 'react-native';
import colors from "../../Config/colors";

const styles = StyleSheet.create({
  actualImage: {
    height: 200, 
    width: null 
  },
  titleview: {
   flexDirection:"column"
  },
  titlestyle: {
    fontSize:18,
    fontWeight:"bold"
   }
});

export default styles;