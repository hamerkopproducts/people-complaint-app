import { StyleSheet } from 'react-native';
import theme from './ThemeStyle';
import colors from '../../Config/colors';

export default styles = StyleSheet.create({
    contentWrapper: {
        backgroundColor: theme.BACKGROUND_COLOR_LIGHT,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    h2: {
        letterSpacing: 1,
        color: theme.TITLE_TEXT_DARK,
        padding: 0,
        width: '100%',
        paddingVertical: 10,
        paddingHorizontal: 15,
        fontSize: 30,
        borderBottomWidth: 1,
        borderBottomColor: theme.BORDER_COLOR,
        fontFamily: theme.FONT_LT,
    },
    formFieldWrapper: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: '85%'
    },
    inputPromptText: {
        fontSize: 18,
        marginTop: 20,
        alignSelf: 'flex-start',
        fontFamily: theme.FONT_MD_CN,
        color: '#666666'
    },
    textInput: {
        height: 50,
        width: '100%',
        borderWidth: 1,
        borderColor: theme.BORDER_COLOR,
        marginVertical: 10,
        backgroundColor: 'white',
        fontSize: 20,
        paddingHorizontal: 10,
        margin: 0,
        fontFamily: theme.FONT_MD_CN,
    },
    errorText: {
        margin: 0,
        fontSize: 18,
        color: 'red',
        fontFamily: theme.FONT_MD_CN,
    },
    card: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        minHeight: 60,
        width: '100%',
        padding: 10
    },
    primaryBtn: {
        paddingHorizontal: 15,
        marginTop: 15,
        height: 40,
        alignSelf: 'flex-end',
        justifyContent: 'center',
        backgroundColor: theme.PRIMARY_BUTTON_COLOR
    },
    submitButton: {
        height: 30,
        minWidth: 50,
        marginHorizontal: 15,
        backgroundColor: theme.BUTTON_BLUE,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        alignSelf: 'center',
    },
    submitButtonText: {
        color: 'white',
        fontSize: 18,
        lineHeight: 18,
        fontFamily: theme.FONT_MD_CN,
    },
    contentText: {
        fontSize: 18,
        color: '#333'
    },
    counterBtn: {
        width: 50,
        height: 40,
        backgroundColor: 'white',
        borderColor: '#ccc',
        borderWidth: 1,
        margin: 0,
        padding: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    counterBtnText: {
        fontSize: 30,
        fontFamily: theme.FONT_MD_CN
    },
    bottomActionWrapper: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'flex-end',
        alignItems: 'center',
        paddingVertical: 8,
        paddingRight: 15,
        borderTopWidth: 1,
        borderTopColor: "#e3e3e3",
        backgroundColor: '#e7e7e7'
    },
    tableContentText: {
        fontSize: 20,
        color: '#333',
        paddingVertical: 10,
        fontFamily: theme.FONT_LT_CN
    },

    fontNormalCn: {
        fontFamily: theme.FONT_NM_CN
    },
    fontLight: {
        fontFamily: theme.FONT_LT
    },
    fontLightCn: {
        fontFamily: theme.FONT_LT_CN
    },
    fontBold: {
        fontFamily: theme.FONT_BD
    },
    fontBoldCn: {
        fontFamily: theme.FONT_BD_CN
    },
    fontMedium: {
        fontFamily: theme.FONT_MD
    },
    fontMediumCn: {
        fontFamily: theme.FONT_MD_CN
    },
    toastWrapper: {
        backgroundColor: theme.TOAST_BACKGROUND_COLOR,
    },
    toastText: {
        color: theme.TOAST_TEXT_COLOR,
        textTransform: 'capitalize'
    },
    toastBtn: {
        backgroundColor: colors.DUNKIN_PINK,
    }
})
