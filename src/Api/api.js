import axios from 'axios';
import urls from '../Config/urls';
//import { stGetUser, stGetCsrf } from '../Utils/storage';

import { Toast } from 'native-base';
import styles from './../Screens/styles/CommonStyles';
import { getAccessToken } from '../Utils/store';

export const ROOT_URL = urls.serverUrl;
export const PREFIX = urls.prefix;
export let LOCALE = 'ml';
export const PAGE_LIMIT = urls.perPage;

let axios_instance = axios.create({
  baseURL: ROOT_URL,
  data: {},
});

axios_instance.interceptors.request.use(
  async function (config) {
    try {
      let accessToken = getAccessToken();
      if(accessToken !== "") {
        config.headers['Authorization'] = 'Bearer ' + accessToken; 
      }
      console.log(config);
      return config;
    } catch (error) {
      console.log(error);
    }
  },
  function (error) { },
);

export default axios_instance;

const withRestUrl = url => `${ROOT_URL}${PREFIX}${url}`;

export const getLoginUrl = () => withRestUrl(`login`);
export const getLogoutUrl = () => withRestUrl(`logout`);
export const postComplaintUrl = () => withRestUrl(`complaint`); 
export const getComplaintDetailUrl = (id) => withRestUrl(`complaintDetails/${id}`); 
export const getComplaintUrl = () => withRestUrl(`list-complaint`); 
//export const getWardUrl = () => withRestUrl(`list-ward?language=${LOCALE}`); 
export const getWardUrl = () => withRestUrl(`list-ward?language=en`); 

export const isEmpty = data => {
  return Object.keys(data).length === 0 ? true : false;
};

export const isNotNull = data => {
  return data !== null ? true : false;
};

export const isArrEmpty = data => {
  return data !== undefined && data.length === 0 ? true : false;
};

export const isValid = data => {
  return data !== undefined && data !== '' ? true : false;
};

export const isArray = data => {
  return Object.prototype.toString.call(data).indexOf('Array') > -1
    ? true
    : false;
};

export const isObject = data => {
  return Object.prototype.toString.call(data).indexOf('Object') > -1
    ? true
    : false;
};

export const getImagePath = imageUrl => {
  let imagePath = urls.serverUrl + imageUrl;
  return imagePath;
};

export const processText = text => {
  let processedText = text.replace(/ /g, "_").toLowerCase();
  return processedText;
}

export const getAggregate = (count, total) => {
  return Math.round((count / total) * 100);
}

export const getTotalCount = (response) => {
  let totalCount = 0;
  if (response.length > 0) {
    response.map(item => {
      totalCount = totalCount + parseInt(item.count);
    });
  }
  return totalCount;
}

export const showToast = (message, btnTxt) => {
  const TOAST_POSITION = 'top';
  const TOAST_DURATION = 3000;
  Toast.show({
    text: message,
    buttonText: btnTxt,
    position: TOAST_POSITION,
    duration: TOAST_DURATION,
    style: styles.toastWrapper,
    buttonTextStyle: styles.toastText,
    buttonStyle: styles.toastBtn,
  });

}