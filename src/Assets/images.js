const images = {
  no_image: require('./Images/no-image.png'),
  splash: require('./Images/splash.png'),
  no_profile: require('./Images/no_profile.png')
}

export default images;
