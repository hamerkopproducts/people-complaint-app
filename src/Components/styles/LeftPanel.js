import { StyleSheet } from 'react-native';
import colors from '../../Config/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: colors.WHITE,
  },
  icon: {
    backgroundColor: colors.DUNKIN_PINK,
  },
  nav_selected: {
    color: colors.DUNKIN_PINK
  },
  closeIcon: {
    color: colors.DUNKIN_PINK
  }

});

export default styles;
