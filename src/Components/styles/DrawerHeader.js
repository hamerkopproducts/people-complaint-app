import { StyleSheet } from 'react-native';
import colors from '../../Config/colors';

const styles = StyleSheet.create({

    headerBackground: {
        backgroundColor: colors.DUNKIN_PINK,
    },    
    headerTitle: {
        color: colors.WHITE,
        fontSize: 18,
        textTransform: 'uppercase'
    },
    headerTint: {
        color: colors.DUNKIN_PINK
    },
    iconStyle: {
        color: colors.WHITE,
    }

});

export default styles;
