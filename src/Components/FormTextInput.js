import React, { Component } from 'react';
import { StyleSheet, TextInput, TextInputProps } from "react-native";
import colors from "../Config/colors";

let Props = TextInputProps;

class FormTextInput extends Component {
 constructor(props) {
super(props);
 }
  
  render() {
    const { style, ...otherProps } = this.props;
    
    return (
      <TextInput placeholderTextColor={colors.BLACK}
        selectionColor={colors.DODGER_BLUE}
        style={[styles.textInput, style]}
        {...otherProps}
      />
    );
  }
}

const styles = StyleSheet.create({
  textInput: {
    height: 50,
    borderColor: colors.SILVER,
    borderBottomWidth: StyleSheet.hairlineWidth,
    marginBottom: 5,
    fontSize: 16,
    color: colors.BLACK
  }
});

export default FormTextInput;  