import React, { Component } from 'react';
import {
    Content,
    Text,
    ListItem,
    Icon,
    Left,
    Button,
    List,
    Body,
    Right,
    Thumbnail
} from 'native-base';
import images from '../Assets/images';
import { connect } from 'react-redux';
import NavigationService from '../Providers/NavigationService';
import { DrawerActions } from 'react-navigation-drawer';
import Config from '../Utils/Config';
import RouteNames from '../Navigations/RouteNames';
import styles from './styles/LeftPanel';
import { strings } from '../../locales/i18n';
import { logOutUser } from '../Actions';

class LeftPanel extends Component {
    constructor(props) {
        super(props)

    }

    drawerPrefix = () => {
        const { navigation } = this.props;
        navigation.dispatch(DrawerActions.toggleDrawer());
    }

    navigateToComplaints = () => {
        this.drawerPrefix();
        NavigationService.navigate(RouteNames.ComplaintList, { title: strings('navigation.title.complaint_list') });
    }

    nextStep = () => {
        const { navigation } = this.props;
        navigation.navigate(RouteNames.Intro);
    };

    onLogout = async () => {
        try {
            this.drawerPrefix();
            this.props.logOutUser(this.nextStep);
        } catch (error) {
            console.log(error);
        }
    };

    getCurrentRouteName() {
        let routes = this.props.navigation.state.routes;
        let route = routes[routes.length - 1].routes;
        if (route.length > 0) {
            return route[route.length - 1].routeName;
        }
        return null;
    }

    render() {
        const { navigation, user, displayName } = this.props;
        let profileAvathar = <Thumbnail source={images.no_profile} />;
        let currentRoute = this.getCurrentRouteName();

        return (
            <Content>
                <List>
                    <ListItem icon>
                        <Left></Left>
                        <Body></Body>
                        <Right>
                            <Icon name='ios-close'
                                style={styles.closeIcon}
                                onPress={() => { this.drawerPrefix(); }} />
                        </Right>
                    </ListItem>
                    {(displayName !== "") &&
                        <ListItem avatar>
                            <Left>
                                {profileAvathar}
                            </Left>
                            <Body>
                                <Text>{displayName}</Text>
                                <Text note>{user.email}</Text>
                            </Body>
                        </ListItem>
                    }

                    <ListItem icon>
                        <Left>
                            <Button style={styles.icon}>
                                <Icon active name='ios-mail-unread' />
                            </Button>
                        </Left>
                        <Body>
                            <Text onPress={() => this.navigateToComplaints()}
                                style={(currentRoute === 'ComplaintList') ? styles.nav_selected : ""}>{strings('leftPanel.complaint_list')}</Text>
                        </Body>
                    </ListItem>

                    {(displayName !== "") &&
                        <ListItem icon>
                            <Left>
                                <Button style={styles.icon}>
                                    <Icon active name='ios-log-out' />
                                </Button>
                            </Left>
                            <Body>
                                <Text onPress={() => this.onLogout()}>{strings('leftPanel.logout')}</Text>
                            </Body>
                        </ListItem>
                    }

                    <ListItem icon>
                        <Left></Left>
                        <Body></Body>
                        <Right><Text note> {strings('leftPanel.version', {
                            number: Config.version
                        })}</Text></Right>
                    </ListItem>
                </List>

            </Content>
        );
    }
}

const mapStateToProps = ({ auth }) => {
    return { ...auth };
};


export default connect(
    mapStateToProps,
    { logOutUser },
)(LeftPanel);