import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import colors from "../Config/colors";



class Button extends Component {

  render() {
    const { label, onPress } = this.props;
    return (
      <TouchableOpacity style={styles.container} onPress={onPress}>
        <Text style={styles.text}>{label}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: '#4050b5',
    marginBottom: 12,
    paddingVertical: 12,
    borderRadius: 40,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: "rgba(255,255,255,0.7)",
    height:55
  },
  text: {
    color: colors.WHITE,
    textAlign: "center",
    fontSize: 22,
    
  }
});

export default Button;