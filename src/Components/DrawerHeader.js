import React, { Component } from 'react';
import { Icon } from 'native-base';
import { TouchableOpacity } from 'react-native';
import { DrawerActions } from 'react-navigation-drawer';
import styles from './styles/DrawerHeader';

class DrawerHeader extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const {navigation} = this.props;
        return (
            <TouchableOpacity>
                <Icon name="ios-menu" style={styles.iconStyle} onPress={() => { navigation.dispatch(DrawerActions.toggleDrawer()) }} />
            </TouchableOpacity>
        );
    }
}

export default DrawerHeader;